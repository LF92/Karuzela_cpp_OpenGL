/**************************
 * Includes
 *
 **************************/

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glut.h>
/**************************
 * Function Declarations
 *
 **************************/

void trojkaty() 
{
	glPushMatrix();
		GLUquadricObj *slup_;
		glColor3f( 0.00,0.20,0.52 );
		slup_ = gluNewQuadric();
		glRotatef(-90.0, 1, 0, 0);
		glTranslatef(0.0f, 0.0f, -0.8f);
		gluCylinder(slup_, 0.4, 0.1, 2.0, 32, 32);
		gluDeleteQuadric(slup_);
	glPopMatrix();
}

void szescian()
{
			glPushMatrix();
			glBegin (GL_QUADS);

            //1 r�owy
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, 0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, -0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, -0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, 0.3f, 0.3f);
            //2 ��ty
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, 0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, -0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, -0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, 0.3f, -0.3f);
            //3 zielony
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, 0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, -0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, -0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, 0.3f, -0.3f);
            //4 czerwony
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, 0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, 0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, 0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, 0.3f, 0.3f);
            //5 blekit
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, 0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, 0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, -0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, -0.3f, 0.3f);
            //6 bialy
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, -0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, -0.3f, 0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (-0.3f, -0.3f, -0.3f);
            glColor3f( 0.42,0.56,0.54 );   glVertex3f (0.3f, -0.3f, -0.3f);
            glEnd ();
            glPopMatrix();
}


void male_budki()
{
	glPushMatrix();
		glTranslatef(0.0f, 0.2f, 0.0f);
			
			glPushMatrix();
				glColor3f( 1.00,0.00,0.00 );
				glTranslatef(-1.4f, -2.1f, 0.0f);
				glutSolidCube(1.2);
			glPopMatrix();
	glPopMatrix();
}

void drzewo()
{
	glPushMatrix();
	glTranslatef(12.0f, 0.0f, -9.0f);
	
		glPushMatrix();
			GLUquadricObj *konar;
			glColor3f( 0.30,0.25,0.04 );
			konar = gluNewQuadric();
			glRotatef(-90.0, 1, 0, 0);
			glTranslatef(-4.0f, -5.0f, -5.0f);
			gluCylinder(konar, 0.5, 0.2, 3.0, 40, 40);
			gluDeleteQuadric(konar);
		glPopMatrix();
	
		glPushMatrix();
			glColor3f( 0.34,0.71,0.09 );
			glTranslatef(-3.4f, -1.2f, 5.0f);
			glutSolidSphere(1.0, 20, 16);
		glPopMatrix();
	
		glPushMatrix();
			glColor3f( 0.07,0.51,0.09 );
			glTranslatef(-4.5f, -1.2f, 5.0f);
			glutSolidSphere(1.0, 20, 16);
		glPopMatrix();
	
		glPushMatrix();
			glColor3f( 0.13,0.87,0.16 );
			glTranslatef(-3.95f, -0.5f, 5.0f);
			glutSolidSphere(1.0, 20, 16);
		glPopMatrix();
		
	glPopMatrix();
}

void budki()
{
	glPushMatrix();
	glTranslatef(0.0f, 0.2f, 0.0f);
		glPushMatrix();
			glColor3f( 1.00,0.00,0.00 );
			glTranslatef(0.0f, -1.8f, 0.0f);
			glutSolidCube(1.7);
		glPopMatrix();
			//kolo tylne 
		glPushMatrix();
			glColor3f( 0.49,0.49,0.49 );
			glTranslatef(0.3f, -2.5f, 0.9f);
			glutSolidTorus(0.2, 0.2, 10, 20);
		glPopMatrix();
			
		glPushMatrix();
			glColor3f( 0.49,0.49,0.49 );
			glTranslatef(-1.4f, -2.5f, 0.6f);
			glutSolidTorus(0.2, 0.2, 10, 20);
		glPopMatrix();
			//kolo tylne, druga strona
		glPushMatrix();
			glColor3f( 0.49,0.49,0.49 );
			glTranslatef(0.3f, -2.5f, -0.9f);
			glutSolidTorus(0.2, 0.2, 10, 20);
		glPopMatrix();
			
		glPushMatrix();
			glColor3f( 0.49,0.49,0.49 );
			glTranslatef(-1.4f, -2.5f, -0.9f);
			glutSolidTorus(0.2, 0.2, 10, 20);
		glPopMatrix();
			
		glPushMatrix();
			glColor3f( 0.33,0.29,0.02 );
			glTranslatef(-0.1f, -1.6f, 0.48f);
			glutWireCube(0.8);
		glPopMatrix();
			
		glPushMatrix();
			glColor3f( 0.84,1.00,1.00 );
			glTranslatef(-0.1f, -1.6f, 0.48f);
			glutSolidCube(0.8f);
		glPopMatrix();
			
		glPushMatrix();
			glColor3f( 0.33,0.29,0.02 );
			glTranslatef(-0.1f, -1.6f, -0.48f);
			glutWireCube(0.8);
		glPopMatrix();
			
		glPushMatrix();
			glColor3f( 0.84,1.00,1.00 );
			glTranslatef(-0.1f, -1.6f, -0.48f);
			glutSolidCube(0.8f);
		glPopMatrix();
			//przednia szybka
		glPushMatrix();
			glColor3f( 0.84,1.00,1.00 );
			glTranslatef(-0.36f, -1.6f, 0.0f);
			glutSolidCube(1.0f);
		glPopMatrix();

    glPopMatrix();
}

void swiatlo()
{
	const GLfloat light_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	const GLfloat light_diffuse[] = {1.0f, 1.0f, 1.0f, 0.0f};
	const GLfloat light_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	const GLfloat light_position[] = {1.0f, 1.0f, 1.0f, 0.0f};
	
	glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

}

void podloga()
{
	glPushMatrix();

		glBegin(GL_TRIANGLES);
			glNormal3f( 0.0, 0.0, 0.0 );
    		for( GLfloat z = -10.0; z < 20.0; z += 1 )
    		{
    			for( GLfloat x = -10.0; x < 30.0; x += 1 )
    			{
        			glColor3f ( 0.08,0.34,0.01 );   glVertex3f (x+0.0f, -0.1f, z+1.0); 
            		glColor3f ( 0.11,0.45,0.02 );   glVertex3f (x+1.0f, -0.1f, z+1.0); 
            		glColor3f ( 0.13,0.55,0.02 );   glVertex3f (x+0.0f, -0.1f, z+0.0); 
            		glColor3f ( 0.13,0.55,0.02 );   glVertex3f (x+1.0f, -0.1f, z+1.0); 
            		glColor3f ( 0.11,0.45,0.02 );   glVertex3f (x+1.0f, -0.1f, z+0.0); 
            		glColor3f ( 0.08,0.34,0.01 );   glVertex3f (x+0.0f, -0.1f, z+0.0); 
    			}
			}
   		 glEnd();
   		 
    glPopMatrix();
}

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
WPARAM wParam, LPARAM lParam);
void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC);
void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC);


/**************************
 * WinMain
 *
 **************************/

int WINAPI WinMain (HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpCmdLine,
                    int iCmdShow)
{
    WNDCLASS wc;
    HWND hWnd;
    HDC hDC;
    HGLRC hRC;        
    MSG msg;
    BOOL bQuit = FALSE;
    float theta = 0.0f;
    int w = 750;
    int h = 750;
	double kat1 = 0.0;
	double deltath=1.0f;
	double deltat=1.0f;
	double oddal=1.0f;
	const int n = 8;
 	const double kat = 360.0 / n;
  	const int r = 5;
  	int tym = 1;
    /* register window class */
    wc.style = CS_OWNDC;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor (NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = "GLSample";
    RegisterClass (&wc);

    /* create main window */
    hWnd = CreateWindow (
      "GLSample", "OpenGL Sample", 
      WS_CAPTION | WS_POPUPWINDOW | WS_VISIBLE,
      0, 0, w, h,
      NULL, NULL, hInstance, NULL);
      
    /* enable OpenGL for the window */
    EnableOpenGL (hWnd, &hDC, &hRC);
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 45.0f, (float) w / (float) h, 1.0, 100 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

/*########################################################################################
/*########################################################################################
/*########################################################################################
    /* program main loop */
    while (!bQuit)
    {
        /* check for messages */
        if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
        {
            /* handle or dispatch messages */
            if (msg.message == WM_QUIT) {
            bQuit = TRUE;
            } else if(msg.message == WM_KEYDOWN) {
            switch(( int ) msg.wParam ) {
            case VK_LEFT:
            // Process the Left Arrow key.
            deltat-=1.0f;
            break;
            case VK_RIGHT:
            // Process the Right Arrow key.
            deltat+=1.0f;
             break;
             case VK_UP:
             // Process the Up Arrow key.
             // deltath+=1.0f;
             oddal-=1.0f;
              break;
              case VK_DOWN:
              // Process the Down Arrow key.
              //deltath-=1.0f;
              oddal+=1.0f;
               break;
               /*
               case VK_PRIOR:
               // Process the Page Up key.
               oddal-=1.0f;
               break;
               case VK_NEXT:
               // Process the Page Down key.
               oddal+=1.0f;
               break;
               */
               case VK_RETURN:
               	oddal=1.0f;
               	deltat=1.0f;
               	deltath=1.0f;
               	break;
               // Process the Enter key.
               
               break;
               } 
               } else {
               TranslateMessage (&msg);
               DispatchMessage (&msg);
            }
        }
        else
        {
            /* OpenGL animation code goes here */

            glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_COLOR_MATERIAL);
			glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
			
            //glPushMatrix ();
           // glRotatef (theta, 0.0f, 0.0f, 0.0f);

	glPushMatrix ();

		glEnable(GL_LIGHT0);
		swiatlo();

glPushMatrix();	
	glTranslatef(0.0f, 0.0f, -20.0f);
	drzewo();	
glPopMatrix();

glPushMatrix();	
	glTranslatef(-21.0f, -0.5f, -30.0f);
	drzewo();	
glPopMatrix();

glTranslatef(0.0f, 0.0f, -21.0f +oddal);

		
glPushMatrix();
	GLUquadricObj *slup_duzy;
	glColor3f( 0.27,0.37,0.38 );
	slup_duzy = gluNewQuadric();
	glRotatef(-90.0, 1, 0, 0);
	glTranslatef(-0.1f, 0.0f, -4.0f);
	gluCylinder(slup_duzy, 2.0, 0.6, 3.0, 40, 40);
	gluDeleteQuadric(slup_duzy);
glPopMatrix();

		//	glRotatef (-theta, 0.05f, 1.0f,0.05f);



			glRotatef (-theta, 0.1f, 1.0f,0.1f);
			
			for(int i=0; i<(n); ++i) 
			{
				szescian();
				
glPushMatrix();
	GLUquadricObj *slup_maly;
	glColor3f( 0.53,0.66,0.67 );
	slup_maly = gluNewQuadric();
	glRotatef(-90.0, 1, 0, 0);
	glTranslatef(0.0f, 0.0f, -3.0f);
	gluCylinder(slup_maly, 0.5, 0.1, 4.0, 32, 32);
	gluDeleteQuadric(slup_maly);
glPopMatrix();


glPushMatrix();
	GLUquadricObj *slupek;
	glColor3f( 1.00,1.00,0.00 );
	slupek = gluNewQuadric();
	glRotatef(-90.0, 1, 0, 0);
	glTranslatef(0.0f, 0.0f, -3.0f);
	gluCylinder(slupek, 0.1, 0.1, 6.0, 32, 32);
	gluDeleteQuadric(slupek);
glPopMatrix();

	  			glRotatef (kat, 0.0f, 1.0f, 0.0f);
  				glTranslatef(0.0f, 0.0f, r);
			
				trojkaty();	
				male_budki();
				budki();

				glRotatef (0.0f, 0.0f, 1.0f, -r);
				glTranslatef(0.0f, 0.0f, -r);
				kat1*=-1.0;
     		  
			   // LINIE OD SRODKA
			   // *********************************
			   
			   glPushMatrix();
		   		glLineWidth(100);
			    glBegin(GL_LINES);
			    
				glColor3f( 0.59,0.65,0.55 );	glVertex3f(0.0f, 0.0f, 0.0f);	glVertex3f(0.0f, -0.5f, 5.0f);
	  		    glEnd();
	  		    glPopMatrix();
	  		   // *********************************
	  		     glPushMatrix();
		   		glLineWidth(100);
			    glBegin(GL_LINES);
			    
				glColor3f( 0.75,0.78,0.72 );	glVertex3f(0.0f, 0.0f, 0.0f);	glVertex3f(0.0f, 0.9f, 5.0f);
	  		    glEnd();
	  		    glPopMatrix();
	  		     // *********************************
	  		     	  		     glPushMatrix();
		   		glLineWidth(100);
			    glBegin(GL_LINES);
			    
				glColor3f( 1.00,1.00,0.00 );	glVertex3f(0.0f, 3.0f, 0.0f);	glVertex3f(0.0f, 1.0f, 5.0f);
	  		    glEnd();
	  		    glPopMatrix();
	  		    
	  		     // *********************************
				
	  		}
			kat1+=deltath;
			
			glEnable(GL_LIGHT0);
						
			swiatlo();
			glPopMatrix ();
			
			
            glLoadIdentity();

            // ********** BEGIN PODLOGA ********** //
            glTranslatef(-5.0f, -4.0f, -20.0f);
			
			glPushMatrix();
				glDisable(GL_LIGHTING);
			
				podloga();
			
			glPopMatrix ();
			
            glLoadIdentity();
            // ********** END PODLOGA ********** //

            SwapBuffers (hDC);

            theta += deltat;
            Sleep (20);
        }
    }

    /* shutdown OpenGL */
    DisableOpenGL (hWnd, hDC, hRC);

    /* destroy the window explicitly */
    DestroyWindow (hWnd);

    return msg.wParam;
}


/********************
 * Window Procedure
 *
 ********************/

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
                          WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_CREATE:
        return 0;
    case WM_CLOSE:
        PostQuitMessage (0);
        return 0;

    case WM_DESTROY:
        return 0;

    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            PostQuitMessage(0);
            return 0;
        }
        return 0;

    default:
        return DefWindowProc (hWnd, message, wParam, lParam);
    }
}


/*******************
 * Enable OpenGL
 *
 *******************/

void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;

    /* get the device context (DC) */
    *hDC = GetDC (hWnd);

    /* set the pixel format for the DC */
    ZeroMemory (&pfd, sizeof (pfd));
    pfd.nSize = sizeof (pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | 
      PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;
    iFormat = ChoosePixelFormat (*hDC, &pfd);
    SetPixelFormat (*hDC, iFormat, &pfd);

    /* create and enable the render context (RC) */
    *hRC = wglCreateContext( *hDC );
    wglMakeCurrent( *hDC, *hRC );

}


/******************
 * Disable OpenGL
 *
 ******************/

void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent (NULL, NULL);
    wglDeleteContext (hRC);
    ReleaseDC (hWnd, hDC);
}
